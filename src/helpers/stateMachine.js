import { Machine } from 'xstate'
import {
  COLOR_MENU,
  RGB_MENU,
  WITHOUT_MENUS,
  SHOW_RGB_MENU,
  SHOW_COLOR_MENU,
  CLOSE,
} from '../constants'

export function menusColorPickerMashine() {
  return Machine({
    id: 'colorPicker',
    initial: WITHOUT_MENUS,
    states: {
      [WITHOUT_MENUS]: {
        on: {
          [SHOW_RGB_MENU]: RGB_MENU,
          [SHOW_COLOR_MENU]: COLOR_MENU,
        },
      },
      [RGB_MENU]: {
        on: {
          [CLOSE]: WITHOUT_MENUS,
          [SHOW_COLOR_MENU]: COLOR_MENU,
        },
      },
      [COLOR_MENU]: {
        on: {
          [CLOSE]: WITHOUT_MENUS,
          [SHOW_RGB_MENU]: RGB_MENU,
        },
      },
    },
  })
}
