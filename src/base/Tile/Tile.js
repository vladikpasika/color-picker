import React, { memo } from 'react'
import './Tile.css'

function Tile({ children, className }) {
  return <div className={`shadow-lg ${className}`}>{children}</div>
}
export default memo(Tile)
