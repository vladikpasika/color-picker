import { useEffect } from 'react'
import hexRgb from 'hex-rgb'
import { useClickAway } from 'react-use'
import { WITHOUT_MENUS } from '../constants'

export function useCloseMenuOnClickAway({ ref, onCancel, value }) {
  useClickAway(ref, ({ target }) => {
    if (target.getAttribute('data') === 'picker-icon') {
      return
    }
    if (value !== WITHOUT_MENUS) {
      onCancel()
    }
  })
}

export function useMapPropsToState({ setColor, value }) {
  useEffect(() => {
    setColor(hexRgb(value))
  }, [setColor, value])
}
