import React, { memo } from 'react'

import RGBMenu from '../RGBMenu/RGBMenu'
import ColorsMenu from '../ColorsMenu/ColorsMenu'
import { COLOR_MENU, RGB_MENU } from '../../constants'

function Menus({
  menu,
  onRGBChange,
  onSelect,
  onSubmit,
  onCancel,
  color,
  colors,
}) {
  switch (menu) {
    case COLOR_MENU:
      return (
        <ColorsMenu
          className="max-w-xs-50 arrow-right-8 bg-white mt-1"
          colors={colors}
          onSelect={onSelect}
        />
      )
    case RGB_MENU:
      return (
        <RGBMenu
          className="max-w-xs-70 arrow-right-2 bg-white mt-1"
          onRGBChange={onRGBChange}
          onSubmit={onSubmit}
          onCancel={onCancel}
          color={color}
        />
      )
    default:
      return null
  }
}

export default memo(Menus)
