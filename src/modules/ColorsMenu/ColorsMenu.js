import React, { memo } from 'react'
import classnames from 'classnames'

import Tile from '../../base/Tile/Tile'
import ColorSquare from '../../base/ColorSquare/ColorSquare'

function ColorMenu({ className, colors, onSelect }) {
  return (
    <Tile className={classnames('relative arrow-up', className)}>
      {colors.map(({ value, label }) => (
        <div
          className="flex items-center justify-between border-b border-solid border-gray-200"
          key={value}
          onClick={() => onSelect(value)}
        >
          <span className="text-gray-700 mr-1 font-medium p-1">{label}</span>
          <ColorSquare color={value} className="m-1" />
        </div>
      ))}
    </Tile>
  )
}

export default memo(ColorMenu)
