import React, { memo, useState, useRef, useCallback, useMemo } from 'react'
import { useMachine } from '@xstate/react'
import rgbHex from 'rgb-hex'
import hexRgb from 'hex-rgb'

import './ColorPicker.css'
import ColorInput from '../../base/ColorInput/ColorInput'
import { menusColorPickerMashine } from '../../helpers/stateMachine'
import Menus from '../Menus/Menus'
import { SHOW_RGB_MENU, SHOW_COLOR_MENU, CLOSE } from '../../constants'
import {
  useCloseMenuOnClickAway,
  useMapPropsToState,
} from '../../helpers/hooks'

function ColorPicker({ value, onChange, colors }) {
  const ref = useRef(null)
  const [current, send] = useMachine(menusColorPickerMashine())
  const [color, setColor] = useState(hexRgb(value))

  const memoizedonCancelHandler = useCallback(() => {
    send(CLOSE)
    setColor(hexRgb(value))
  }, [send, value])

  useCloseMenuOnClickAway({
    ref,
    onCancel: memoizedonCancelHandler,
    value: current.value,
  })

  useMapPropsToState({ setColor, value })

  const memoizedRGBColor = useMemo(() => {
    return `rgb(${Object.values(color).join(',')})`
  }, [color])

  const memoizedHexColor = useMemo(() => rgbHex(memoizedRGBColor), [
    memoizedRGBColor,
  ])

  const memoizedOncolorMenuClickHandler = useCallback(
    () => send(SHOW_COLOR_MENU),
    [send]
  )
  const memoizedOnRGBMenuClickHandler = useCallback(() => send(SHOW_RGB_MENU), [
    send,
  ])
  const memoizedOnRGBPickerChangeHandler = useCallback(
    ({ target: { name, value } }) => {
      setColor(values => ({ ...values, [name]: value }))
    },
    []
  )
  const memoizedOnChangeMenuHandler = useCallback(() => {
    onChange(memoizedHexColor)
    send(CLOSE)
  }, [memoizedHexColor, onChange, send])

  const memoizedOnSubmitColorHandler = useCallback(
    value => {
      onChange(value)
      send(CLOSE)
    },
    [onChange, send]
  )

  return (
    <div className="flex flex-col items-end bg-main p-2">
      <ColorInput
        onRGBMenuClick={memoizedOnRGBMenuClickHandler}
        onColorMenuClick={memoizedOncolorMenuClickHandler}
        color={memoizedRGBColor}
        value={`#${memoizedHexColor}`}
      />
      <div className="relative flex flex-col items-end" ref={ref}>
        <Menus
          menu={current.value}
          onRGBChange={memoizedOnRGBPickerChangeHandler}
          color={color}
          colors={colors}
          onSelect={memoizedOnSubmitColorHandler}
          onSubmit={memoizedOnChangeMenuHandler}
          onCancel={memoizedonCancelHandler}
        />
      </div>
    </div>
  )
}

export default memo(ColorPicker)
