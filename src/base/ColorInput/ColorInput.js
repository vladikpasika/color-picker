import React, { memo } from 'react'
import classnames from 'classnames'

import ColorSquare from '../ColorSquare/ColorSquare'
import './ColorInput.css'

function ColorInput({
  className,
  classNames,
  onRGBMenuClick,
  onColorMenuClick,
  color,
  value,
}) {
  return (
    <div
      className={classnames(
        'flex border border-solid border-gray-200 max-w-xs',
        className
      )}
    >
      <input
        className={classnames(classNames.input)}
        type="text"
        disabled
        value={value}
      />
      <div
        className="border-r border-solid border-gray-200 bg-white"
        onClick={onColorMenuClick}
      >
        <ColorSquare color={color} className="m-2" data="picker-icon" />
      </div>
      <div
        className="border-r border-solid border-gray-200 flex item-center justify-center bg-white"
        onClick={onRGBMenuClick}
      >
        <div className="m-2 w-2 h-2 arrow-down relative" data="picker-icon" />
      </div>
    </div>
  )
}
ColorInput.defaultProps = {
  classNames: {
    input: 'flex-grow text-gray-700 font-medium px-1',
  },
}
export default memo(ColorInput)
