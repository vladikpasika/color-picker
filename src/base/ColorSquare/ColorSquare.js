import React, { memo } from 'react'

function ColorSquare({ color, className, ...rest }) {
  return (
    <div
      className={`h-2 w-2 ${className}`}
      style={{ backgroundColor: color }}
      {...rest}
    ></div>
  )
}

export default memo(ColorSquare)
