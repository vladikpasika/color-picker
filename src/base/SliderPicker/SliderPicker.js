import React, { memo } from 'react'
import './SliderPicker.css'

function SliderPicker({ className, inputId, value, updateColor, ...rest }) {
  return (
    <div className={className}>
      <input
        id={inputId}
        type="range"
        min="0"
        max="255"
        steps="1"
        value={value}
        onChange={updateColor}
        {...rest}
      />
    </div>
  )
}

export default memo(SliderPicker)
