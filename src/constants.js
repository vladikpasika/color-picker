export const COLOR_MENU = 'COLOR_MENU'
export const RGB_MENU = 'RGB_MENU'
export const WITHOUT_MENUS = 'WITHOUT_MENUS'
//actions
export const SHOW_RGB_MENU = 'SHOW_RGB_MENU'
export const SHOW_COLOR_MENU = 'SHOW_COLOR_MENU'
export const CLOSE = 'CLOSE'
