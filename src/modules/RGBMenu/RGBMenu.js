import React, { memo } from 'react'
import classnames from 'classnames'

import Tile from '../../base/Tile/Tile'
import SliderPicker from '../../base/SliderPicker/SliderPicker'

function Menu({ className, onRGBChange, color, onSubmit, onCancel }) {
  return (
    <Tile className={classnames('relative p-1 arrow-up', className)}>
      <div className="flex items-center mt-2">
        <span className="text-gray-700 mr-1 font-medium">R</span>
        <SliderPicker
          className="red flex"
          inputId="red"
          updateColor={onRGBChange}
          value={color.red}
          name="red"
        />
      </div>
      <div className="flex items-center mt-2">
        <span className="text-gray-700 mr-1 font-medium">G</span>
        <SliderPicker
          className="green flex"
          inputId="green"
          updateColor={onRGBChange}
          value={color.green}
          name="green"
        />
      </div>
      <div className="flex items-center mt-2 font-medium">
        <span className="text-gray-700 mr-1">B</span>
        <SliderPicker
          className="blue flex"
          inputId="blue"
          updateColor={onRGBChange}
          value={color.blue}
          name="blue"
        />
      </div>
      <div className="flex justify-end w-full mt-2">
        <button
          className="bg-gray-400 text-gray-700 px-1 font-medium"
          onClick={onCancel}
        >
          Cancel
        </button>
        <button
          className="text-white bg-green-700 px-1 ml-1 font-medium px-2"
          onClick={onSubmit}
        >
          OK
        </button>
      </div>
    </Tile>
  )
}
export default memo(Menu)
