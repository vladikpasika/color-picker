import React, { memo, useCallback, useState } from 'react'
import ColorPicker from './modules/ColorPicker/ColorPicker'
import './App.css'

const defaultColor = '#FF5733'

const colors = [
  {
    label: 'RED',
    value: '#ff0000',
  },
  {
    label: 'YELLOW',
    value: '#ffff00',
  },
  {
    label: 'GREEN',
    value: '#00ff00',
  },
  {
    label: 'BLUE',
    value: '#0000ff',
  },
]

function App() {
  const [color, setColor] = useState(defaultColor)

  const memoizedOnChangeHandler = useCallback(value => {
    setColor(value)
  }, [])
  return (
    <div className="flex flex-col w-full h-screen items-center justify-center">
      <ColorPicker
        value={color}
        onChange={memoizedOnChangeHandler}
        colors={colors}
      />
    </div>
  )
}

export default memo(App)
